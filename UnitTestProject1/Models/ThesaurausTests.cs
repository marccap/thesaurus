﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using ThesaurusViewer.Models;

namespace ThesaurusViewerTests.Models
{
    [TestClass]
    public class ThesaurausTests
    {
        [TestMethod]
        public void ThesaurusTests()
        {
            IThesaurus t = new Thesaurus();
            Assert.IsNull(t.GetWords().ElementAtOrDefault(0));
            t.AddSynonyms(new List<string> { "gå", "promenera", "traska" });
            t.AddSynonyms(new List<string> { "springa", "rusa", "kubba", "sprinta" });
            Assert.AreEqual(t.GetSynonyms("gå").ElementAt(2), "traska");
            Assert.AreEqual(t.GetWords().ElementAt(6), "sprinta");
            Assert.IsNull(t.GetWords().ElementAtOrDefault(7));
        }
    }
}
