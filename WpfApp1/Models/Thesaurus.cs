﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ThesaurusViewer.Models
{
    public class Thesaurus : IThesaurus
    {
        private readonly Dictionary<string, List<string>> _synonyms =
            new Dictionary<string, List<string>>(StringComparer.InvariantCultureIgnoreCase);

        public void AddSynonyms(IEnumerable<string> synonyms)
        {
            var trimmedWords = synonyms.Select(x => x.Trim()).ToList();

            if (trimmedWords.Any(word => word.All(Char.IsSymbol) || word.All(Char.IsDigit)))
                throw new FormatException("Input words must only contain letters!");

            foreach(var synonym in trimmedWords)
                _synonyms.Add(synonym, trimmedWords);
        }

        public IEnumerable<string> GetSynonyms(string word)
        {
            return _synonyms[word] ?? Enumerable.Empty<string>();
        }

        public IEnumerable<string> GetWords()
        {
            return _synonyms.Keys;
        }
    }
}
