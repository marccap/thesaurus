﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using ThesaurusViewer.Models;

namespace ThesaurusViewer.ViewModels
{
    internal class ViewModelThesaurus : ViewModelBase
    {
        private IThesaurus _thesaurus = new Thesaurus();

        public ViewModelThesaurus()
        {
            _thesaurus.AddSynonyms(new List<string> {"gå", "promenera", "spatsera"});
            _thesaurus.AddSynonyms(new List<string> {"springa", "kubba", "rusa"});
            
            AddWord = new DelegateCommand(AddWordExecute);

        }

        private void AddWordExecute()
        {
            PendingWords.Add(WordToAdd);
        }

        public ObservableCollection<string> AllWords { get; } = new ObservableCollection<string>();
        public ObservableCollection<string> SearchResults { get; } = new ObservableCollection<string>();
        public ObservableCollection<string> PendingWords { get; } = new ObservableCollection<string>();

        public string WordToAdd { get; set; }
        public ICommand AddWord { get; }
        public ICommand AddSynonym { get; }
    }
}
