﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using ThesaurusViewer.Models;

namespace ThesaurusViewer.Views
{
    public partial class ViewerCodeBehind
    {

        private readonly IThesaurus Thesaurus;

        public ViewerCodeBehind()
        {
            Thesaurus = new Thesaurus();
            InitializeComponent();
        }

        private void AddWordButton_Click(object sender, RoutedEventArgs e)
        {
            AddWord();
        }

        private void AddWord()
        {
            if(Word.Text.Length == 0)
                return;

            WordList.Items.Add(Word.Text);
            Word.Clear();
        }

        private void AddSynButton_Click(object sender, RoutedEventArgs e)
        {
            AddSyn();
        }

        private void AddSyn()
        {
            if(WordList.Items.Count < 2)
            {
                MessageBox.Show("At least two words are needed in a synonym");
                return;
            }

            var words = new List<string>();

            foreach(var item in WordList.Items)
                words.Add(item.ToString());
            WordList.Items.Clear();


            Thesaurus.AddSynonyms(words);

            AllWords.Items.Clear();
            foreach(var word in Thesaurus.GetWords())
                AllWords.Items.Add(word);
        }

        private void Find()
        {
            SearchResults.Items.Clear();

            foreach(var result in Thesaurus.GetSynonyms(SearchBox.Text))
                SearchResults.Items.Add(result);
        }

        private void FindButton_Click(object sender, RoutedEventArgs e)
        {
            Find();
        }

        private void SearchBox_KeyUp(object sender, KeyEventArgs e)
        {
            // Uncomment next line for search as you type
            if(e.Key == Key.Enter)
                Find();
        }

        private void Word_KeyUp(object sender, KeyEventArgs e)
        {
            if((Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) && e.Key == Key.Enter)
                AddSyn();
            else if(e.Key == Key.Enter)
                AddWord();
        }
    }
}
