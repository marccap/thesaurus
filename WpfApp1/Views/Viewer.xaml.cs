﻿using ThesaurusViewer.ViewModels;

namespace ThesaurusViewer.Views
{
    public partial class Viewer
    {
        public Viewer()
        {
            InitializeComponent();
            DataContext = new ViewModelThesaurus();
        }
    }
}
